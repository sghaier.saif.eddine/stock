const express = require('express')
const app = express()
const port = 3000

const db = require('./database/db') 

const usersDb = db.collection('users'); 

const liam = usersDb.doc('lragozzine'); 

 liam.set({
	first: 'Liamzizizi',
	last: 'Ragozzine',
	address: '133 5th St., San Francisco, CA',
	birthday: '05/13/1990',
	age: '30'
   });

    usersDb.doc('vpeluso').set({
	first: 'Vanessa',
	last: 'Peluso',
	address: '49 Main St., Tampa, FL',
	birthday: '11/30/1977',
	age: '47'
   });



app.get('/', (req,res)=> {
    res.send("hello world !!")
})

app.listen(port, ()=> {
    console.log(`Server is running on port ${port}`)
})