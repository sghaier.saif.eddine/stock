const fs = require('firebase-admin');

const serviceAccount = require('./stock-management-a8e13-firebase-adminsdk-imp56-c4473066e9.json');

fs.initializeApp({
 credential: fs.credential.cert(serviceAccount)
});

//const db = fs.firestore();

module.exports = fs.firestore();